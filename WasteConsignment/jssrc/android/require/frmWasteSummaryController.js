define(function(){
	var controller = require("userfrmWasteSummaryController");
	var controllerActions = ["frmWasteSummaryControllerActions"];

	for(var i = 0; i < controllerActions.length; i++){
		var actions = require(controllerActions[i]);
		for(var key in actions){
			controller[key] = actions[key];
		}
	}

	return controller;
})