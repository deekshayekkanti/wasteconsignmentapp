define({
    appInit: function(params) {
        skinsInit();
        kony.mvc.registry.add("com.WasteConsignment.AddressSummary", "AddressSummary", "AddressSummaryController");
        kony.application.registerMaster({
            "namespace": "com.WasteConsignment",
            "classname": "AddressSummary",
            "name": "com.WasteConsignment.AddressSummary"
        });
        kony.mvc.registry.add("com.WasteConsignment.appHeader", "appHeader", "appHeaderController");
        kony.application.registerMaster({
            "namespace": "com.WasteConsignment",
            "classname": "appHeader",
            "name": "com.WasteConsignment.appHeader"
        });
        kony.mvc.registry.add("com.wasteConsignmnet.burgerMenu", "burgerMenu", "burgerMenuController");
        kony.application.registerMaster({
            "namespace": "com.wasteConsignmnet",
            "classname": "burgerMenu",
            "name": "com.wasteConsignmnet.burgerMenu"
        });
        kony.mvc.registry.add("flxProductInfo", "flxProductInfo", "flxProductInfoController");
        kony.mvc.registry.add("frmWasteSummary", "frmWasteSummary", "frmWasteSummaryController");
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {},
    appmenuseq: function() {
        new kony.mvc.Navigation("frmWasteSummary").navigate();
    }
});