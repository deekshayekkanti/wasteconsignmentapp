define(function() {
    return function(controller) {
        var burgerMenu = new kony.ui.FlexContainer({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "burgerMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        burgerMenu.setDefaultUnit(kony.flex.DP);
        burgerMenu.add();
        return burgerMenu;
    }
})