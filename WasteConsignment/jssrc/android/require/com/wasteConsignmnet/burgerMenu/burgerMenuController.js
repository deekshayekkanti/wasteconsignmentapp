define(function() {
    var controller = require("com/wasteConsignmnet/burgerMenu/userburgerMenuController");
    var actions = require("com/wasteConsignmnet/burgerMenu/burgerMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});