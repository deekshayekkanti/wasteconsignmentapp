define(function() {
    return function(controller) {
        var appHeader = new kony.ui.FlexContainer({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "appHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        appHeader.setDefaultUnit(kony.flex.DP);
        var flxHeaderBG = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxHeaderBG",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "sknFlxBlackSolid",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxHeaderBG.setDefaultUnit(kony.flex.DP);
        var imgLeftArrow = new kony.ui.Image2({
            "centerY": "50%",
            "height": "50dp",
            "id": "imgLeftArrow",
            "isVisible": true,
            "left": "16dp",
            "skin": "slImage",
            "src": "lftarrow.png",
            "top": "0dp",
            "width": "24dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHeaderTxt = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblHeaderTxt",
            "isVisible": true,
            "left": "72dp",
            "skin": "sknLblRegWhite160",
            "text": "Waste Summary",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var imgBurgerMenu = new kony.ui.Image2({
            "centerY": "50%",
            "height": "24dp",
            "id": "imgBurgerMenu",
            "isVisible": false,
            "left": "16dp",
            "skin": "slImage",
            "top": "0dp",
            "width": "24dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderBG.add(imgLeftArrow, lblHeaderTxt, imgBurgerMenu);
        appHeader.add(flxHeaderBG);
        return appHeader;
    }
})