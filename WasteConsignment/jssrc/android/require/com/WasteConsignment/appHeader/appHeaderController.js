define(function() {
    var controller = require("com/WasteConsignment/appHeader/userappHeaderController");
    var actions = require("com/WasteConsignment/appHeader/appHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "isVisible1", function(val) {
            this.view.imgLeftArrow.isVisible = val;
        });
        defineGetter(this, "isVisible1", function() {
            return this.view.imgLeftArrow.isVisible;
        });
        defineSetter(this, "isVisible2", function(val) {
            this.view.imgBurgerMenu.isVisible = val;
        });
        defineGetter(this, "isVisible2", function() {
            return this.view.imgBurgerMenu.isVisible;
        });
        defineSetter(this, "text", function(val) {
            this.view.lblHeaderTxt.text = val;
        });
        defineGetter(this, "text", function() {
            return this.view.lblHeaderTxt.text;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});