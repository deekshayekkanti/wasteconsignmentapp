define(function() {
    return {
        "properties": [{
            "name": "isVisible1",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "isVisible2",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "text",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": [],
        "events": []
    }
});