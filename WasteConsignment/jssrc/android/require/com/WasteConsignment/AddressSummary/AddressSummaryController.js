define(function() {
    var controller = require("com/WasteConsignment/AddressSummary/userAddressSummaryController");
    var actions = require("com/WasteConsignment/AddressSummary/AddressSummaryControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});