define(function() {
    return function(controller) {
        var AddressSummary = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "AddressSummary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3%",
            "skin": "slFbox",
            "top": "0dp",
            "width": "94%"
        }, {}, {});
        AddressSummary.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "3%",
            "skin": "slFbox",
            "top": "15dp",
            "width": "94%",
            "zIndex": 1
        }, {}, {});
        flxContent.setDefaultUnit(kony.flex.DP);
        var lblCustomerTxt = new kony.ui.Label({
            "id": "lblCustomerTxt",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLightGrey100",
            "text": "Customer",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "R.F & G.M Eades Dental Practice",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblAddressLine1 = new kony.ui.Label({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "36 Kirkgate",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblAddressLine2 = new kony.ui.Label({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "Tadcaster",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblCity = new kony.ui.Label({
            "id": "lblCity",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "North Yorkshire",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblPostCode = new kony.ui.Label({
            "id": "lblPostCode",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "LS24 9AD",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        flxContent.add(lblCustomerTxt, lblName, lblAddressLine1, lblAddressLine2, lblCity, lblPostCode);
        AddressSummary.add(flxContent);
        return AddressSummary;
    }
})