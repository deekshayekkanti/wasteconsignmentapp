define("frmWasteSummary", function() {
    return function(controller) {
        function addWidgetsfrmWasteSummary() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var appHeader = new com.WasteConsignment.appHeader({
                "clipBounds": true,
                "height": "60dp",
                "id": "appHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            var scrFlxContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "scrFlxContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "reverseLayoutDirection": false,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "60dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            scrFlxContent.setDefaultUnit(kony.flex.DP);
            var flxConsignmentDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxConsignmentDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "6%",
                "skin": "slFbox",
                "top": "16dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxConsignmentDetails.setDefaultUnit(kony.flex.DP);
            var lblConsignmnetNoTxt = new kony.ui.Label({
                "id": "lblConsignmnetNoTxt",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLightGrey100",
                "text": "Waste Consignment Number",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblConsignmentValue = new kony.ui.Label({
                "id": "lblConsignmentValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBlack130",
                "text": "EXIMM/OIALM",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxConsignmentDetails.add(lblConsignmnetNoTxt, lblConsignmentValue);
            var AddressSummary = new com.WasteConsignment.AddressSummary({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "AddressSummary",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "3%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "skin": "sknFlxLightGreyBorder",
                "top": "12dp",
                "width": "94%"
            }, {}, {});
            var segProductInfo = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblContainerTxt": "Container",
                    "lblContainerValue": "UN3401 or UN3402 Container various sizes",
                    "lblEWCC0de": "EWC Code",
                    "lblEWCValue": "180110",
                    "lblProductDesc": "Amalgam Capsule Waste(AMC)",
                    "lblProductTxt": "Product",
                    "lblQuantityTxt": "Quantity",
                    "lblQuantityValue": "5",
                    "lblWeightTxt": "Label",
                    "lblWeightValue": "20 KGs"
                }, {
                    "lblContainerTxt": "Container",
                    "lblContainerValue": "UN3401 or UN3402 Container various sizes",
                    "lblEWCC0de": "EWC Code",
                    "lblEWCValue": "180110",
                    "lblProductDesc": "Amalgam Capsule Waste(AMC)",
                    "lblProductTxt": "Product",
                    "lblQuantityTxt": "Quantity",
                    "lblQuantityValue": "5",
                    "lblWeightTxt": "Label",
                    "lblWeightValue": "20 KGs"
                }, {
                    "lblContainerTxt": "Container",
                    "lblContainerValue": "UN3401 or UN3402 Container various sizes",
                    "lblEWCC0de": "EWC Code",
                    "lblEWCValue": "180110",
                    "lblProductDesc": "Amalgam Capsule Waste(AMC)",
                    "lblProductTxt": "Product",
                    "lblQuantityTxt": "Quantity",
                    "lblQuantityValue": "5",
                    "lblWeightTxt": "Label",
                    "lblWeightValue": "20 KGs"
                }],
                "groupCells": false,
                "id": "segProductInfo",
                "isVisible": true,
                "left": "3%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxProductInfo",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "15dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxBorder": "flxBorder",
                    "flxProductContent": "flxProductContent",
                    "flxProductInfo": "flxProductInfo",
                    "lblContainerTxt": "lblContainerTxt",
                    "lblContainerValue": "lblContainerValue",
                    "lblEWCC0de": "lblEWCC0de",
                    "lblEWCValue": "lblEWCValue",
                    "lblProductDesc": "lblProductDesc",
                    "lblProductTxt": "lblProductTxt",
                    "lblQuantityTxt": "lblQuantityTxt",
                    "lblQuantityValue": "lblQuantityValue",
                    "lblWeightTxt": "lblWeightTxt",
                    "lblWeightValue": "lblWeightValue"
                },
                "width": "94%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCustomerSummary = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnCustomerSummary",
                "isVisible": true,
                "left": "3%",
                "skin": "sknBtnBlueBG150",
                "text": "GO TO CUSTOMER SUMMARY",
                "top": "0dp",
                "width": "94%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddProduct = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "35dp",
                "id": "btnAddProduct",
                "isVisible": true,
                "left": "3%",
                "skin": "sknBtnBlueTxt150",
                "text": "ADD PRODUCT",
                "top": "12dp",
                "width": "94%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmpty = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxEmpty",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmpty.setDefaultUnit(kony.flex.DP);
            flxEmpty.add();
            scrFlxContent.add(flxConsignmentDetails, AddressSummary, segProductInfo, btnCustomerSummary, btnAddProduct, flxEmpty);
            flxMain.add(appHeader, scrFlxContent);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmWasteSummary,
            "enabledForIdleTimeout": false,
            "id": "frmWasteSummary",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});