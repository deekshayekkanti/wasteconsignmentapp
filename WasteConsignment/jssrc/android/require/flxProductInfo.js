define("flxProductInfo", function() {
    return function(controller) {
        var flxProductInfo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxProductInfo.setDefaultUnit(kony.flex.DP);
        var flxBorder = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 15,
            "clipBounds": true,
            "id": "flxBorder",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "sknFlxLightGreyBorder",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxBorder.setDefaultUnit(kony.flex.DP);
        var flxProductContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 16,
            "clipBounds": true,
            "id": "flxProductContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "3%",
            "skin": "slFbox",
            "top": "16dp",
            "width": "94%",
            "zIndex": 1
        }, {}, {});
        flxProductContent.setDefaultUnit(kony.flex.DP);
        var lblProductTxt = new kony.ui.Label({
            "id": "lblProductTxt",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLightGrey100",
            "text": "Product",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "7dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblProductDesc = new kony.ui.Label({
            "id": "lblProductDesc",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "Amalgam Capsule Waste(AMC)",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblEWCC0de = new kony.ui.Label({
            "id": "lblEWCC0de",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLightGrey100",
            "text": "EWC Code",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblEWCValue = new kony.ui.Label({
            "id": "lblEWCValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "180110",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblWeightTxt = new kony.ui.Label({
            "id": "lblWeightTxt",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLightGrey100",
            "text": "Weight",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblWeightValue = new kony.ui.Label({
            "id": "lblWeightValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "20 KGs",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblContainerTxt = new kony.ui.Label({
            "id": "lblContainerTxt",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLightGrey100",
            "text": "Container",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "10dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblContainerValue = new kony.ui.Label({
            "id": "lblContainerValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "UN3401 or UN3402 Container various sizes",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblQuantityTxt = new kony.ui.Label({
            "id": "lblQuantityTxt",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLightGrey100",
            "text": "Quantity",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblQuantityValue = new kony.ui.Label({
            "id": "lblQuantityValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblBlack130",
            "text": "5",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        flxProductContent.add(lblProductTxt, lblProductDesc, lblEWCC0de, lblEWCValue, lblWeightTxt, lblWeightValue, lblContainerTxt, lblContainerValue, lblQuantityTxt, lblQuantityValue);
        flxBorder.add(flxProductContent);
        flxProductInfo.add(flxBorder);
        return flxProductInfo;
    }
})