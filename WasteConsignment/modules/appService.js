//Type your code here

function appServiceHandler(params){
  
  printObject(params, "Inside appServcieHandler params");
  gblLaunchMode				= params.launchmode;
  printObject(gblLaunchMode, "Inside appServcieHandler gblLaunchMode");
  if(params.launchmode === 3){
    var decodedParamValue	= decodeURIComponent(params.launchparams.ParamstoSends); 
  	var decodedJsonValue	= JSON.parse(decodedParamValue);  
  	printObject(decodedJsonValue, "Inside appServcieHandler decodedJsonValue");
    
    if(decodedJsonValue.visitStatus === "NEW"){
      var index				= decodedJsonValue.indexVal;
      var indexOne			= index % 3;
      var wasteNum			= "EXEIMM/OIALM" + index.toString();
      
      gblInprogressVisit	= {
                "HWNDocument": {
                    "WCNNumber": wasteNum,
                    "WasteAcceptance": "",
                    "CustomerSign": "",
                    "CustomerName": ""
                },
                "HWNCustomer": [gblCustomerData[indexOne]],
                "HWNProduct": [{
                      "ProductCode": "180110",
                      "productDesc": "Amalgam Capsule Waste(ACW)",
                      "Quantity": "5",
                      "Container": "UN3401 or UN3402 Container various sizes",
                      "Weight": "20 Kgs",
                      "ExpectedProduct": false
                    }]
      };
      
    } else {
      gblInprogressVisit	= kony.store.getItem("gblInprogressVisit");
    }
    
    printObject(gblInprogressVisit, "Inside appServcieHandler gblInprogressVisit");
    kony.store.setItem("gblInprogressVisit", gblInprogressVisit);

    return "frmWasteSummary";
  }
}