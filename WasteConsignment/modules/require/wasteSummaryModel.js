define(function () {
   
    return {
        
      getCustomerSummaryDetails: function(productDataArr){
        printObject(productDataArr,"Inside getCustomerSummaryDetails productDataArr");
        try{
          var customerSummaryDetails						= {};
          customerSummaryDetails.wasteTransferDetails		= [{ "template"					: "flxWasteTransferInfo",
            													 "lblConsignmentNumValue" 	: "EXEIMM/OIALM"
                                                              }];
                                                             
          for (var i = 0; i < productDataArr.length; i++){
            var prodObj		= productDataArr[i];
            var rowJson		= { 
              					"template"					: "flxProductSummary",
            					"lblConsignmentNumValue" 	: "EXEIMM/OIALM",
              					"lblProductDesc"			: prodObj.productDesc,
              					"lblQuantityValue"			: prodObj.Quantity,
              					"lblEWCValue"				: prodObj.ProductCode,
              					"lblContainerValue"			: prodObj.Container,
              					"lblWeightValue"			: prodObj.Weight
            				  };
            customerSummaryDetails.wasteTransferDetails.push(rowJson);
          }
          
          var customerData									= gblInprogressVisit.HWNCustomer[0];
          var tempArr										= [{ "template"					: "flxCarrierSummary",
            													"lblConsignmentNumValue" 	: "EXEIMM/OIALM"
                                                              },
                                                              { "template"					: "flxEnterMail",
            													 "lblConsignmentNumValue" 	: "EXEIMM/OIALM",
                                                               	"txtEmail"					: {"text" : "", "setFocus": true},
                                                               "lblValidEmail"				: {"isVisible" : false}
                                                              },
                                                              { "template"					: "flxSignature",
            													"lblConsignmentNumValue" 	: "EXEIMM/OIALM",
                                                               	"lblConsignorName"			: customerData.CustomerName,
                                                               	"lblConsignorAddressLine1"	: customerData.AddressLine1,
                                                               	"lblConsignorAddressLine2"	: customerData.AddressLine2,
                                                               	"lblConsignorCity"			: customerData.AddressLine3,
                                                               	"lblConsignorPostCode"		: customerData.PostCode,
                                                               	"txtCustomerName"					: {"text" : "", "setFocus": true},
                                                               	"btnDelete"					: {"skin" : "sknBtnDeleteDisabled"},
                                                               	"lblSignHere"				: {"skin" : "sknLblLightGrey150"},
                                                               	"imgSignature"				: ""
                                                              }];
          customerSummaryDetails.wasteTransferDetails		= customerSummaryDetails.wasteTransferDetails.concat(tempArr);
          return customerSummaryDetails;
        } catch (exception){
          printObject(exception,"Inside getCustomerSummaryDetails exception");
        }
      },
      
    getAddedProductData : function(params){
         var productData = [];
          if(! isEmpty(params["addedProduct"])){
              var addedProduct = params["addedProduct"];
               printObject(params,"wasteSummaryParams");
               productData.push(addedProduct);         
               return productData;
          }
      },
      
      constructWasteSummarySegmentData :   function(addressInfo,arrProducts,productTemplate,isSelectDepotVisible){
       printObject(addressInfo,"AddressInfo");        
       printObject(arrProducts,"arrProducts Data"); 
        
       var prodCount   = arrProducts.length;
       var prodObj     = {}; 
       var segData     = []; 
       // set customer info 
       var customerObj  =  {
                             "lblCustomer" 		: { "text" : "Customer","skin":"sknLblLightGrey90","isVisible":true},
                             "lblName" 	        : { "text" : addressInfo.CustomerName,"skin":"sknLblBlack100TxtOpacity87","top":"0dp"},
                             "lblAddressLine1" 	: { "text" : addressInfo.AddressLine1,"skin":"sknLblBlack100TxtOpacity87"},
                             "lblAddressLine2" 	: { "text" : addressInfo.AddressLine2,"skin":"sknLblBlack100TxtOpacity87"},  
                             "lblCity"          : { "text" : addressInfo.AddressLine3,"skin":"sknLblBlack100TxtOpacity87"},
                             "lblPostCode"      : { "text" : addressInfo.PostCode,"skin":"sknLblBlack100TxtOpacity87"},
                             "btnSelectDepot"   : { "isVisible" : isSelectDepotVisible },
                             "template"         : "flxAddressInfo"
                           };
       segData.push(customerObj);
        
      // set product info 
       for(var index=0 ; index<prodCount; index++){
            prodObj = { 
              "lblProductTxt" 	    : { "text" : "Product","skin":"sknLblLightGrey90"},
              "lblEWCCode" 		    : { "text" : "EWC Code","skin":"sknLblLightGrey90"},
              "lblWeightTxt" 		: { "text" : "Weight","skin":"sknLblLightGrey90"},
              "lblContainerTxt" 	: { "text" : "Container","skin":"sknLblLightGrey90"},
              "lblQuantityTxt" 	    : { "text" : "Quantity","skin":"sknLblLightGrey90"},
              "lblProductDesc" 	    : { "text" : arrProducts[index].productDesc ,"skin":"sknLblBlack100TxtOpacity87"},
              "lblQuantityValue" 	: { "text" : arrProducts[index].Quantity,"skin":"sknLblBlack100TxtOpacity87"},
              "lblEWCValue"		    : { "text" : arrProducts[index].ProductCode ,"skin":"sknLblBlack100TxtOpacity87"}, 
              "lblWeightValue"      : { "text" : arrProducts[index].Weight,"skin":"sknLblBlack100TxtOpacity87"},
              "lblContainerValue"   : { "text" : arrProducts[index].Container,"skin":"sknLblBlack100TxtOpacity87"},
              "imgDelete"           : { "isVisible" : arrProducts[index].ExpectedProduct},
              "lblAddition"         : { "isVisible" : arrProducts[index].ExpectedProduct,"text":"Additional Collection"},
              "template"            :   productTemplate 
            }; 
            segData.push(prodObj);                   
        } 
         return segData;
      },
      
      constructWidgetDataMap : function(){    
          printObject("constructWidgetDataMap");
          var widgetDataMap = {
                                  //tmpProductInfo widget
                                  "imgDelete"          : "imgDelete",
                                  "lblProductTxt"      : "lblProductTxt",
                                  "lblProductDesc"     : "lblProductDesc",
                                  "lblEWCCode"         : "lblEWCCode",
                                  "lblEWCValue"        : "lblEWCValue",
                                  "lblWeightTxt"       : "lblWeightTxt",
                                  "lblWeightValue"     : "lblWeightValue",
                                  "lblContainerTxt"    : "lblContainerTxt",
                                  "lblContainerValue"  : "lblContainerValue",
                                  "lblQuantityTxt"     : "lblQuantityTxt",
                                  "lblQuantityValue"   : "lblQuantityValue", 
                                  "lblAddition"        : "lblAddition",
            
                                  //tmpAddress Info
                                  "lblCustomer" 		: "lblCustomer",
                                  "lblName" 	        : "lblName",
                                  "lblAddressLine1" 	: "lblAddressLine1",
                                  "lblAddressLine2" 	: "lblAddressLine2",   
                                  "lblCity"            :  "lblCity",
                                  "lblPostCode"        :  "lblPostCode",
                                  "btnSelectDepot"     :  "btnSelectDepot"
                                 }
          
                return widgetDataMap;
        
      },
      
      getPoliceViewData: function(){
        printObject("Inside getPoliceViewData");
        var policeViewData	= [];
        var rowDataOne		= 	[{ "template"					: "flxDriverDetails",
                                    "lblDriverName"				: "Keith Middlebrook"}];
        
        policeViewData.push(rowDataOne);
        
        for(var i = 0; i < gblConsignmentData.length; i++){
          
          var wasteSumObj	= gblConsignmentData[i];
          
          var headerObj		= wasteSumObj.HWNDocument;
          var customerObj	= wasteSumObj.HWNCustomer[0];
          var productArr	= wasteSumObj.HWNProduct;
          
          var rowDataArr	 = [{ "template" 		: "flxWasteCollectedFrom",
                                  "lblName"  		: customerObj.CustomerName,
                                  "lblAddressLine1"	: customerObj.AddressLine1,
                                  "lblAddressLine2"	: customerObj.AddressLine2,
                                  "lblCity"			: customerObj.AddressLine3,
                                  "lblPostCode"		: customerObj.PostCode,
                               	  "lblCustomerName"	: headerObj.CustomerName,
                                  "imgSignature"	: {"base64" : headerObj.CustomerSign}
         					  }];
          
          for(var k = 0; k < productArr.length; k++){
            var prodObj		= productArr[k];
            var wasteCol	= "";
            if(k === 0){
              wasteCol		= "Waste Collected";
            } else {
              wasteCol		= "";
            }
             
            
            var prodRowJson	= { "template" 			: "flxWasteCollected",
                               	"lblWasteCollected" : wasteCol,
                                "lblProductDesc"  	: prodObj.productDesc,
                               	"lblQuantityValue"	: prodObj.Quantity,
                               	"lblEWCValue"		: prodObj.ProductCode,
                               	"lblWeightValue"	: prodObj.Weight,
                                "lblContainerValue" : prodObj.Container
            				  };
            
            rowDataArr.push(prodRowJson);
          }
          policeViewData.push(rowDataArr);
        } 
        return policeViewData;
      }
      
    };
});