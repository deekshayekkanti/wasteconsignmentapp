//Type your code here

function printObject(jsonObj, optionalText) {
    if (!isEmpty(optionalText) && optionalText.trim() !== "") {
        kony.print("***WASTE MANAGEMENT*** " + optionalText + ": " + JSON.stringify(jsonObj));
    } else {
        kony.print("***WASTE MANAGEMENT**** " + JSON.stringify(jsonObj));
    }
}


function isResultNotNull(resultTable){
	if (resultTable !== null && resultTable !== undefined && resultTable !== ""){
		return true;
	}else{
		return false;
	}
}

function isEmpty(val){
	return (val === undefined || val === null || val === '' || val.length <= 0 || (typeof(val) === "object" && Object.keys(val).length === 0)) ? true : false;
}

function textBoxIsEmpty(txtObj){
  	printObject(txtObj, "Inside textBoxIsEmpty - txtObj - ");
	return (!isEmpty(txtObj.text) && txtObj.text.trim().length === 0) ? true : false;
}

//Checks whether the string is empty or not
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}


function showLoadingIndicator() {			
	try{
		kony.application.showLoadingScreen("sknLoading",kony.i18n.getLocalizedString("i18n.key.loading"), constants.LOADING_SCREEN_POSITION_ONLY_CENTER , true, true,null); 
	}catch(e){
		kony.print("Error while showing loading indicator :"+e);
	}	
}

function dismissLoadingIndicator() {
    kony.print("Dismiss loading indicator is called");
	kony.application.dismissLoadingScreen();
}

function saveValueInKonyStore(storeKey, storeValue){
  	try {
        printObject(storeKey, " Inside setDataInKonyStore - storeKey - ");
        printObject(storeValue, " Inside setDataInKonyStore - storeValue - ");
        if (storeValue !== null && storeValue !== undefined) {
            kony.store.setItem(storeKey, storeValue);
        } else {
            printObject(" Inside setDataInKonyStore - failed to save in store - Given value is undefined or null - ");
        }  	
  	} catch (e) {
      	logDataInSupportView("setDataInKonyStore", "CODE_EXCEPTION", null, e);
    }
}

function getValueFromKonyStore(storeKey){
  	try {
        printObject(storeKey, " Inside getDataFromKonyStore - storeKey - ");
        var resultVal			= null;
       	resultVal				= kony.store.getItem(storeKey);
        printObject(resultVal, " Inside getDataFromKonyStore - resultVal - ");
        return resultVal;
   	} catch (e) {
      	logDataInSupportView("getDataFromKonyStore", "CODE_EXCEPTION", null, e);
    }
}

function removeValueFromKonyStore(storeKey){
  	try {
        printObject(storeKey, " Inside removeFromKonyStore - storeKey - ");
        kony.store.removeItem(storeKey);
  	} catch (e) {
      	logDataInSupportView("removeFromKonyStore", "CODE_EXCEPTION", null, e);
    }
}


function displayError(err) {
	kony.print("******************Exception Occurred*************"+err);
    dismissLoadingIndicator();
    setCommonPopupData(1,kony.i18n.getLocalizedString("generic_serviceError"),{'popupSingleButtonText':kony.i18n.getLocalizedString("generic_ok")});
}

function navigateToForm(frmId , params){
     var navObject		= new kony.mvc.Navigation(frmId);
      navObject.navigate(params);
}

function doNothing(){
  printObject("Inside doNothing");
}