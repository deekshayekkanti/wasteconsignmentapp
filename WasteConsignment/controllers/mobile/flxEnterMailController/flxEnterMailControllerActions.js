define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTextChange defined for txtEmail **/
    AS_TextField_e3f9af0a06874042a484fdb4a3bb0c53: function AS_TextField_e3f9af0a06874042a484fdb4a3bb0c53(eventobject, changedtext, context) {
        var self = this;
        return self.onTextChangeCustomerEmail.call(this, changedtext);
    },
    /** onDone defined for txtEmail **/
    AS_TextField_ccfbb4607a944d1d955d9398e80c37d5: function AS_TextField_ccfbb4607a944d1d955d9398e80c37d5(eventobject, changedtext, context) {
        var self = this;
        return self.onDoneCustomerEmail.call(this, null);
    }
});