define({ 

 //Type your controller code here 
  _wasteSummaryData			: [],

  
  onNavigate : function(params){
    printObject(gblInprogressVisit,"Inside navigateToWasteSummary");
    this.view.lblConsignmentValue = gblInprogressVisit.HWNDocument.WCNNumber;
    var WasteSummaryModel 	= require("wasteSummaryModel"); 
    var widgetDataMap       = WasteSummaryModel.constructWidgetDataMap();
    printObject(widgetDataMap, "widgetDataMap");
    this.view.segProductInfo.widgetDataMap = widgetDataMap;
    var addressInfo         =  gblInprogressVisit.HWNCustomer;
    var arrProducts         =  gblInprogressVisit.HWNProduct;  
    var segData             =  WasteSummaryModel.constructWasteSummarySegmentData(addressInfo,arrProducts,"flxProductInfo",false);
    printObject(segData, "wasteSummary SegData");
    this.view.segProductInfo.setData(segData); 
  },

  navigateToCustomerSummary: function (){
    printObject("Inside navigateToCustomerSummary");
    try {
      this.view.onBoardTwo.setVisibility(false);
      this.view.btnCustomerSummary.setEnabled(true);
      this.view.btnAddProduct.setEnabled(true);
      var WasteSummaryModel 	= require("wasteSummaryModel");      
   	  _wasteSummaryData			= WasteSummaryModel.getCustomerSummaryDetails(gblInprogressVisit.HWNProduct);
      printObject(_wasteSummaryData,"Inside navigateToCustomerSummary");
      
      var navObject				= new kony.mvc.Navigation("CustomerSummary");
      var params				= {"wasteSummaryData" : _wasteSummaryData};
      navObject.navigate(params);
    } catch(exception){
      displayError(exception);
    }
   },
  
    navigateToAddProduct: function (){
      try {
              var prodData    	= this.view.segProductInfo.data;
              printObject(prodData,"Inside navigateToAddProduct");
              var addedProduct 	= [];
              for(var index = 0; index < prodData.length; index++)
                {
                  addedProduct.push(prodData[index].lblProductDesc);
                }
               var params		= {"productData":addedProduct};
               navigateToForm("frmAddProduct", params);
      } catch(exception){  
              printObject(exception,"Exception in AddProduct");
      }  
  },
  
   onClickDelete  : function(eventObject){
      printObject(eventObject,"Inside onClickDelete");
      var slctRow = eventObject.selectedRowIndex[1];
      printObject(slctRow, "Selected Row Index");
      this.view.segProductInfo.removeAt(slctRow);
      var existingProdData = gblInprogressVisit.HWNProduct;
      existingProdData.splice(slctRow-1, 1);
      gblInprogressVisit.HWNProduct = existingProdData;
     
   },
   
   
   showOnBoardingScreen: function (){
    printObject("Inside showOnBoardingScreen");
    try {
      this.view.onBoardTwo.setVisibility(true);
      this.view.onBoardTwo.btnOKClick	= this.navigateToCustomerSummary;
      this.view.btnCustomerSummary.setEnabled(false);
      this.view.btnAddProduct.setEnabled(false);
  	} catch(exception){
      displayError(exception);
    }
  },
  
  onSelectSegmentMenu: function(eventObj){
    printObject(eventObj, "onSelectSegmentMenu");
    var selRowIndex	= eventObj.selectedRowIndex[1];
    this.view.burgerMenu.left	=	"-100%";
    if(selRowIndex === 1){
      this.view.onBoardConfirm.setVisibility(true);
    } else if(selRowIndex === 0){
      this.view.onBoardConfirm.setVisibility(false);
      var paramtoSend							= {};
	  paramtoSend 								= JSON.stringify(paramtoSend);
  	  kony.print("Inside openApp - paramtoSend: " + paramtoSend);
  	  var ParamstoSend 							= encodeURIComponent(paramtoSend);
	  kony.application.openURL("HygieneApp://com.kony.HygieneApp/?form2go=frmWorkDetail&packageName=com.kony.HygieneApp&ParamstoSends=" + ParamstoSend + "");
    }
  },
  
  confirmVisitRejection: function(){
    printObject("confirmVisitRejection");
    this.view.onBoardConfirm.setVisibility(false);
    var paramtoSend 							= {"custName" : "", "custSign" : "iVBORw0KGgoAAAANSUhEUgAAAtAAAADICAYAAADBTdZDAAACRUlEQVR42u3BMQEAAADCoPVPbQo/oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHgYy0AAAaGfqDUAAAAASUVORK5CYII="};
	paramtoSend 								= JSON.stringify(paramtoSend);
  	kony.print("Inside openApp - paramtoSend: " + paramtoSend);
  	var ParamstoSend 							= encodeURIComponent(paramtoSend);
	kony.application.openURL("HygieneApp://com.kony.HygieneApp/?form2go=frmFinish&packageName=com.kony.HygieneApp&ParamstoSends=" + ParamstoSend + "");
  },
  
  onClickCancelButtonPopUp: function(){
    printObject("Inside onClickCancelButtonPopUp");
    this.view.onBoardConfirm.setVisibility(false);
  }
 });