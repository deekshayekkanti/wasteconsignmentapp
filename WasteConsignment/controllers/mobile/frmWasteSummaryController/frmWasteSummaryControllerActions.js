define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchBackArrow defined for appHeader **/
    AS_UWI_h63b8684fb5c4bc4a2547153743ba72b: function AS_UWI_h63b8684fb5c4bc4a2547153743ba72b(eventobject) {
        var self = this;

        function MOVE_ACTION____ee517374d2e34025a6f95d9c7a06cfff_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____ee517374d2e34025a6f95d9c7a06cfff_Callback
        });
    },
    /** onClick defined for btnCustomerSummary **/
    AS_Button_df765155906e4670aef29121da90e8be: function AS_Button_df765155906e4670aef29121da90e8be(eventobject) {
        var self = this;
        return self.showOnBoardingScreen.call(this);
    },
    /** onClick defined for btnAddProduct **/
    AS_Button_d433b7495c8f45629e9d55f971582d89: function AS_Button_d433b7495c8f45629e9d55f971582d89(eventobject) {
        var self = this;
        this.navigateToAddProduct();
    },
    /** leftBtnOnClick defined for onBoardConfirm **/
    AS_UWI_c090bbc6e4e04c8e86c3224486e88d79: function AS_UWI_c090bbc6e4e04c8e86c3224486e88d79(eventobject) {
        var self = this;
        return self.confirmVisitRejection.call(this);
    },
    /** rightBtnOnClick defined for onBoardConfirm **/
    AS_UWI_g63465d8e78740c491260356b5f74ccf: function AS_UWI_g63465d8e78740c491260356b5f74ccf(eventobject) {
        var self = this;
        return self.onClickCancelButtonPopUp.call(this);
    },
    /** hideMenu defined for burgerMenu **/
    AS_UWI_f055b96fe0f143acbc1fe2a2325e54b9: function AS_UWI_f055b96fe0f143acbc1fe2a2325e54b9(eventobject) {
        var self = this;

        function MOVE_ACTION____e9d80a7b1c6f4fc0a5608f9090777b5a_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____e9d80a7b1c6f4fc0a5608f9090777b5a_Callback
        });
    },
    /** onRowSelectSegMenu defined for burgerMenu **/
    AS_UWI_e1ff7043428b45cdb9840f980471b8f2: function AS_UWI_e1ff7043428b45cdb9840f980471b8f2(eventobject) {
        var self = this;
        return self.onSelectSegmentMenu.call(this, eventobject);
    },
    /** onDeviceBack defined for frmWasteSummary **/
    AS_Form_iaa292bf15d84c83a239fbe3e1652d73: function AS_Form_iaa292bf15d84c83a239fbe3e1652d73(eventobject) {
        var self = this;
        return doNothing.call(this);
    }
});