define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClickBtnFinish defined for appHeaderLogo **/
    AS_UWI_ia05f79f82b844b181b4e8291066dd70: function AS_UWI_ia05f79f82b844b181b4e8291066dd70(eventobject) {
        var self = this;
        return self.onClickFinishVisit.call(this);
    },
    /** onRowClick defined for segCustomerSummary **/
    AS_Segment_h01ce0d4288a408baa93e79c0a883a22: function AS_Segment_h01ce0d4288a408baa93e79c0a883a22(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.onClickCustomerSummarySegment.call(this, eventobject);
    },
    /** onSwipe defined for segCustomerSummary **/
    AS_Segment_c80881a914b04e8fa51524eb03d2ee38: function AS_Segment_c80881a914b04e8fa51524eb03d2ee38(seguiWidget, sectionIndex, rowIndex) {
        var self = this;
        return self.onSwipeCustomerSummarySegment.call(this, rowIndex);
    },
    /** preShow defined for frmCustomerSummary **/
    AS_Form_ee3fa2e415e742c4a2c9ee2ba992fa6b: function AS_Form_ee3fa2e415e742c4a2c9ee2ba992fa6b(eventobject) {
        var self = this;
        return self.preshowCustomerSummary.call(this);
    }
});