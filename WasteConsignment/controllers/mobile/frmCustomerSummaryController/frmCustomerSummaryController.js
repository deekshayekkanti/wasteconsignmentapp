define({ 

 //Type your controller code here 
  _wasteSummaryDetails			: [],
  _numOfRows					: 0,
  _customerDetails				: {"custName" : "", "custSign" : ""},
  
  onNavigate: function(context){
    printObject(context, "Inside onNavigate CustomerSummary - context");
    try {
      if(isResultNotNull(context)){
        _wasteSummaryDetails	= context.wasteSummaryData.wasteTransferDetails;
        _numOfRows				= _wasteSummaryDetails.length;
        this._customerDetails	= {"custName" : "", "custSign" : ""};
        printObject(_numOfRows, "Inside onNavigate CustomerSummary - _numOfRows");
      }
    } catch(exception){
      
    }
  },
  
  preshowCustomerSummary: function(){
    printObject(_wasteSummaryDetails, "Inside preshowCustomerSummary - _wasteSummaryDetails");
    this.view.segCustomerSummary.widgetDataMap = {
      "lblConsignmentNumValue"		: "lblConsignmentNumValue",
      "btnDelete"					: "btnDelete",
      "txtEmail"					: "txtEmail",
      "lblSignHere"					: "lblSignHere",
      "imgSignature"				: "imgSignature",
      "lblProductDesc"				: "lblProductDesc",
      "lblQuantityValue"			: "lblQuantityValue",
      "lblValidEmail"				: "lblValidEmail",
      "txtCustomerName"				: "txtCustomerName",
      "lblConsignorName"			: "lblConsignorName",
      "lblConsignorAddressLine1"	: "lblConsignorAddressLine1",
      "lblConsignorAddressLine2"	: "lblConsignorAddressLine2",
      "lblConsignorCity"			: "lblConsignorCity",
      "lblConsignorPostCode"		: "lblConsignorPostCode",
      "lblEWCValue"					: "lblEWCValue",
      "lblContainerValue"			: "lblContainerValue"
    };
    
    this.view.segCustomerSummary.setData(_wasteSummaryDetails);
    
    this.view.lblConsignmentNumValue.text	=	gblInprogressVisit.HWNDocument.WCNNumber;
  },
  
  onSwipeCustomerSummarySegment: function(rowIndex){
    printObject(rowIndex, "Inside onSwipeCustomerSummarySegment");
     try {
//       if(rowIndex === _numOfRows-1){
//         this.view.lblSwipeToNext.setVisibility(false);
//       } else {
//         this.view.lblSwipeToNext.setVisibility(true);
//       }
    } catch(exception){
      
    }
  },
  
  updateCustomerName: function(widgetInfo,params){
    printObject(params, "Inside updateCustomerName params");
    printObject(this._customerDetails, "Inside updateCustomerName _customerDetails");
    try {
      this._customerDetails.custName	= params.customerName;
      var customerName					= this._customerDetails.custName;
      var customerSign					= this._customerDetails.custSign;
      var lblSignHereSkin				= "";
      var btnDeleteSkin					= "sknBtnDeleteEnabled";
      var imgSignature					= "";
      var showBtnFinish					= false;
      this.updateCustomerNameAndSignature(customerName, customerSign, lblSignHereSkin, btnDeleteSkin, imgSignature, showBtnFinish);
    } catch(exception){
      printObject(exception, "Inside updateCustomerName exception");
    }
  },
  
  updateCustomerSignature: function(widgetInfo,params){
    printObject(params, "Inside updateCustomerSignature params");
    printObject(this._customerDetails, "Inside updateCustomerSignature _customerDetails");
    try {
      this._customerDetails.custSign	= params.customerSign;
      
      var customerName					= this._customerDetails.custName;
      var customerSign					= this._customerDetails.custSign;
      var lblSignHereSkin				= "";
      var btnDeleteSkin					= "sknBtnDeleteEnabled";
      var imgSignature					= {"isVisible" : true, "base64": customerSign};
      var showBtnFinish					= false;
      this.updateCustomerNameAndSignature(customerName, customerSign, lblSignHereSkin, btnDeleteSkin, imgSignature, showBtnFinish);
    } catch(exception){
      printObject(exception, "Inside updateCustomerSignature exception");
    }
  },
  
  updateCustomerNameAndSignature: function(customerName, customerSign, lblSignHereSkin, btnDeleteSkin, imgSignature, showBtnFinish){
    printObject("Inside updateCustomerNameAndSignature");
    try {
		printObject(customerName, "Inside updateCustomerNameAndSignature customerName");
      	printObject(customerSign, "Inside updateCustomerNameAndSignature customerSign");
      
    	if(!isResultNotNull(customerName) && !isResultNotNull(customerSign)){
     		printObject("Inside updateCustomerNameAndSignature IF1"); 
      		lblSignHereSkin		= "sknLblLightGrey150";
     	 	imgSignature		= {"isVisible" : false};
      		btnDeleteSkin		= "sknBtnDeleteDisabled";
      		showBtnFinish		= false;
    	} else if(!isResultNotNull(customerName) && isResultNotNull(customerSign)){
      		printObject("Inside updateCustomerNameAndSignature IF2"); 
      		lblSignHereSkin		= "sknLblLightGrey150";
      		imgSignature		= {"isVisible" : true, "base64": customerSign};
      		btnDeleteSkin		= "sknBtnDeleteEnabled";
          	showBtnFinish		= false;
      
    	} else if(isResultNotNull(customerName) && !isResultNotNull(customerSign)){
      		printObject("Inside updateCustomerNameAndSignature IF3"); 
      		lblSignHereSkin		= "sknLblBlack150";
      		imgSignature		= {"isVisible" : true, src: "imgtrans.png"};
     		btnDeleteSkin		= "sknBtnDeleteDisabled";
      		showBtnFinish		= false;
    	} else {
          	imgSignature		= {"isVisible" : true, "base64": customerSign};
          	btnDeleteSkin		= "sknBtnDeleteEnabled";
          	showBtnFinish		= true;
        }
      	var customerData		= gblInprogressVisit.HWNCustomer[0];
    	var rowJson				= {
            						"lblConsignmentNumValue" 	: "EXEIMM/OIALM",
                   					"btnDelete"					: {"skin" : btnDeleteSkin},
                   					"txtCustomerName"			: {"text" : customerName, setFocus: true},
                           			"lblSignHere"				: {"skin" : lblSignHereSkin},
                           			"imgSignature"				: imgSignature,
          							"lblConsignorName"			: customerData.CustomerName,
                                    "lblConsignorAddressLine1"	: customerData.AddressLine1,
                                     "lblConsignorAddressLine2"	: customerData.AddressLine2,
                                     "lblConsignorCity"			: customerData.AddressLine3,
                                     "lblConsignorPostCode"		: customerData.PostCode
                  				  };
    	this.view.segCustomerSummary.setDataAt(rowJson, _numOfRows-1);
      	this.view.appHeaderLogo.btnFinishVisibility	= showBtnFinish;

    } catch(exception){
      	printObject(exception, "Inside updateCustomerNameAndSignature");
    }
  },
  
  showInvalidEmailText: function(widgetInfo, params){
    printObject(widgetInfo,"Inside showInvalidEmailText widgetInfo");
    printObject(params,"Inside showInvalidEmailText params");
    try{
      var custMail				= params.customerMail;
      printObject(custMail,"Inside showInvalidEmailText custMail");

      var isValidMail			= true;
      if(isResultNotNull(custMail)){
         isValidMail			= kony.string.isValidEmail(custMail);
      }
      printObject(isValidMail,"Inside showInvalidEmailText isValidMail");
      
      var rowJson				= {
        							"txtEmail"					: {"text" 		: custMail},
                                  	"lblValidEmail"				: {"isVisible" 	: !isValidMail}
                               	  }; 
      this.view.segCustomerSummary.setDataAt(rowJson, _numOfRows-2);
    } catch(exception){
      printObject(exception, "Inside showInvalidEmailText");
    }
  },
  
  onClickCustomerSummarySegment: function(eventObj){
    printObject("Inside onClickCustomerSummarySegment");
    try{
      var selItem		= this.view.segCustomerSummary.selectedItems[0];
      var selRowIndex	= this.view.segCustomerSummary.selectedRowIndex[1];
      printObject(selItem,"Inside onClickCustomerSummarySegment selItem");
      printObject(selRowIndex,"Inside onClickCustomerSummarySegment selItem");
      
      if(selRowIndex === _numOfRows-2){
        var params		= {"customerMail" : selItem.txtEmail.text};
        var emptyObj	= {};
        this.showInvalidEmailText(emptyObj, params);
      } else if(selRowIndex === _numOfRows-1){
        var objKeys		= Object.keys(selItem.imgSignature);
        printObject(objKeys,"Inside onClickCustomerSummarySegment objKeys");
        var custImg		= "";
        if(objKeys.includes('base64') > -1){
          custImg		= selItem.imgSignature.base64;
        }
        var params1		= {"customerName" : selItem.txtCustomerName.text, "customerSign" : custImg};
        this.updateCustomerName({}, params1);
      }  
    } catch(exception){
      printObject(exception, "Inside onClickCustomerSummarySegment");
    }
  },
  
  
  onClickFinishVisit: function(){
    printObject(this._customerDetails,"Inside onClickFinishVisit");
    try{
      	
		gblInprogressVisit.HWNDocument.CustomerName	= this._customerDetails.custName;
      	gblInprogressVisit.HWNDocument.CustomerSign	= this._customerDetails.custSign;
      	printObject(gblInprogressVisit, "Inside onClickFinishVisit1");
      	gblConsignmentData.push(gblInprogressVisit);
      	kony.store.setItem("gblConsignmentData", gblConsignmentData);
      	printObject(gblConsignmentData,"Inside onClickFinishVisit2");
      	gblInprogressVisit							= {};
      	kony.store.setItem("gblInprogressVisit", gblInprogressVisit);
      	printObject("Inside onClickFinishVisit3");
       	var paramtoSend 							= this._customerDetails;
		paramtoSend 								= JSON.stringify(paramtoSend);
  		kony.print("Inside openApp - paramtoSend: " + paramtoSend);
  		var ParamstoSend 							= encodeURIComponent(paramtoSend);
		kony.application.openURL("HygieneApp://com.kony.HygieneApp/?form2go=frmFinish&packageName=com.kony.HygieneApp&ParamstoSends=" + ParamstoSend + "");
      	this._customerDetails						= {"custName" : "", "custSign" : ""};
      	this.view.appHeaderLogo.btnFinishVisibility	= false;
      	
      	var navObj	= new kony.mvc.Navigation("frmHome");
      	navObj.navigate();
      
    } catch(exception){
      	printObject(exception, "Inside onClickFinishVisit exception");
    }
  }

  
});