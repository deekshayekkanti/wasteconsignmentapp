define({ 

 //Type your controller code here 

  _custName			: "",
  _textChanged		: false,
  onDoneEditingCustomerName: function(){
  	printObject("Inside onDoneEditingTextEmail");
    var params				= {"customerName" : this._custName};
    printObject(params,"Inside onDoneEditingTextEmail  params");
    if(this_textChanged){
      this_textChanged		= false;
      this.executeOnParent("updateCustomerName", params);
    }
  },
  
  onTextChangeCustomerName: function(changedText){
    printObject(changedText,"Inside onTextChangeTextEmail");
    this._custName			= changedText;
    this_textChanged		= true;
  },

  captureCustomerSignature: function() {
    
  	printObject("Inside captureCustomerSignature");
    var strimg 				= "";
    var SignHere			= "SignHere";
    var Clear				= "Clear";
    var NewBtn				= "Save";
  	var isCustomerRefAvail  = false;
  	var customerRefHint    	= "123";
  	var DateFormat			= "MM-dd-yyyy";
  	var TimeFormat			= "HH:mm:ss";
    var customerReference 	= "123";
    
    try {
        signatureFFI.getSignature(this.signCallBack, strimg, SignHere, Clear, NewBtn, isCustomerRefAvail, customerRefHint, customerReference, DateFormat, TimeFormat);  
    } catch (error) {
        printObject(error, "Inside onClickhboxSign - Exception - ");
    }
  },
  
  signCallBack: function(base64String, unScaledBase64, customerReference){
    try{
      	printObject("Inside signCallBack");
      	var params			= {"customerSign" : base64String};
      	printObject(params,"Inside signCallBack  params");
      	this.executeOnParent("updateCustomerSignature", params);
    } catch (error) {
      	printObject(error, "Inside signCallBack - Exception - ");
    }
  },
  
  deleteSignature: function(){
    try{
    	printObject("Inside deleteSignature");
    	var params			= {"customerSign" : ""};
    	printObject(params,"Inside deleteSignature  params");
    	this.executeOnParent("updateCustomerSignature", params);
    } catch (error) {
        printObject(error, "Inside deleteSignature - Exception - ");
    }
  }

 });