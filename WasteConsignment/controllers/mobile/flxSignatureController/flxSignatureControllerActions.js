define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTextChange defined for txtCustomerName **/
    AS_TextField_gffd8920bb1a4748b2e6cb3912429ea0: function AS_TextField_gffd8920bb1a4748b2e6cb3912429ea0(eventobject, changedtext, context) {
        var self = this;
        return self.onTextChangeCustomerName.call(this, changedtext);
    },
    /** onDone defined for txtCustomerName **/
    AS_TextField_fdf0a4b393a54a29919a8224b3d4a588: function AS_TextField_fdf0a4b393a54a29919a8224b3d4a588(eventobject, changedtext, context) {
        var self = this;
        return self.onDoneEditingCustomerName.call(this);
    },
    /** onClick defined for btnDelete **/
    AS_Button_a276b774682d4d4898cc2af74317bee9: function AS_Button_a276b774682d4d4898cc2af74317bee9(eventobject, context) {
        var self = this;
        return self.deleteSignature.call(this);
    },
    /** onTouchStart defined for imgSignature **/
    AS_Image_cec9dea093684890a5de7272d251e7c1: function AS_Image_cec9dea093684890a5de7272d251e7c1(eventobject, x, y, context) {
        var self = this;
        return self.captureCustomerSignature.call(this);
    }
});