define({ 

 //Type your controller code here 
  _policeViewSegData		: [],
  _currentpage				: 1,
  _totalPages				: 0,
  preShowPoliceView: function(){
    printObject("Inside preShowPoliceView");

    var WasteSummaryModel 		= require("wasteSummaryModel");      
   	this._policeViewSegData		= WasteSummaryModel.getPoliceViewData();
    printObject(this._policeViewSegData, "Inside preShowPoliceView");
    this.view.segPoliceView.widgetDataMap = {
      "lblDriverName"		: "lblDriverName",
      "lblName"				: "lblName",
      "lblAddressLine1"		: "lblAddressLine1",
      "lblAddressLine2"		: "lblAddressLine2",
      "lblCity"				: "lblCity",
      "lblPostCode"			: "lblPostCode",
      "lblCustomerName"		: "lblCustomerName",
      "imgSignature"		: "imgSignature",
      "lblProductDesc"  	: "lblProductDesc",
      "lblQuantityValue"	: "lblQuantityValue",
      "lblEWCValue"			: "lblEWCValue",
      "lblWeightValue"		: "lblWeightValue",
      "lblContainerValue" 	: "lblContainerValue",
      "lblWasteCollected"	: "lblWasteCollected"
    };
    
    this._currentpage		= 1;
    this._totalPages		= this._policeViewSegData.length;
    this.view.segPoliceView.setData(this._policeViewSegData[0]);
    this.hideOrUnhideButtons();
  },
  
  onClickNextButton: function(){
    printObject("Inside onClickNextButton");
    this._currentpage++;
    this.hideOrUnhideButtons();
  },
  
  onClickPrevButton: function(){
    printObject("Inside onClickPrevButton");
    this._currentpage--;
    this.hideOrUnhideButtons();
  },
  
  hideOrUnhideButtons: function(){
    printObject("Inside hideOrUnhideButtons");
    
    if (this._currentpage == 1) {
      this.view.formPagination.isPrevVisible		= false;
      this.view.formPagination.isNextVisible		= true;	
      this.view.formPagination.isNextImgVisible		= true;
      this.view.formPagination.isPrevImgVisible		= false;
    } else if (this._currentpage == this._totalPages) {
       this.view.formPagination.isPrevVisible		= true;
      this.view.formPagination.isNextVisible		= false;	
      this.view.formPagination.isNextImgVisible		= false;
      this.view.formPagination.isPrevImgVisible		= true;
    } else {
      this.view.formPagination.isPrevVisible		= true;
      this.view.formPagination.isNextVisible		= true;
      this.view.formPagination.isNextImgVisible		= true;
      this.view.formPagination.isPrevImgVisible		= true;
    }
    
    if (this._currentpage == 1) {
      this.view.flxConsignmentNumber.setVisibility(false);
      this.view.lblCurrentLoad.text					= "Driver / Depot Details";
    } else {
      this.view.flxConsignmentNumber.setVisibility(true);
      var pageNum									= this._currentpage-1;
      var totalNum									= this._totalPages-1;
      var visitObj									= gblConsignmentData[pageNum-1];
      this.view.lblConsignmentNumValue.text			= visitObj.HWNDocument.WCNNumber;
      this.view.lblCurrentLoad.text					= "Current Load " + pageNum.toString() + " of " + totalNum.toString();
      
    }
    this.view.formPagination.pageNum				= this._currentpage.toString() + " / " + this._totalPages.toString();
    this.view.segPoliceView.setData(this._policeViewSegData[this._currentpage - 1]);
    
  },
  
  navigateToHomeScreen: function(){
    printObject("Inside navigateToHomeScreen");
    var navObj		= new kony.mvc.Navigation("frmHome");
    navObj.navigate();
    
  }

 });