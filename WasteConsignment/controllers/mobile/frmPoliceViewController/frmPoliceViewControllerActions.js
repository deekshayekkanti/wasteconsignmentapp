define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClickHome defined for appHeaderLogo **/
    AS_UWI_cbb84c51419b495586982283ae48feb7: function AS_UWI_cbb84c51419b495586982283ae48feb7(eventobject) {
        var self = this;
        return self.navigateToHomeScreen.call(this);
    },
    /** onClickPrev defined for formPagination **/
    AS_UWI_f2ec159a6ebb466a8d1f5895895afe3a: function AS_UWI_f2ec159a6ebb466a8d1f5895895afe3a(eventobject) {
        var self = this;
        return self.onClickPrevButton.call(this);
    },
    /** onClickNext defined for formPagination **/
    AS_UWI_f21016a0b0c8489589a35bd85b76ff90: function AS_UWI_f21016a0b0c8489589a35bd85b76ff90(eventobject) {
        var self = this;
        return self.onClickNextButton.call(this);
    },
    /** onClickLeftArw defined for formPagination **/
    AS_UWI_e4f0ba2345764e7db126df8f9ad019bd: function AS_UWI_e4f0ba2345764e7db126df8f9ad019bd(eventobject) {
        var self = this;
        return self.onClickPrevButton.call(this);
    },
    /** onClickRightArw defined for formPagination **/
    AS_UWI_fea8dd7561f04dedba5e75e3aa164d10: function AS_UWI_fea8dd7561f04dedba5e75e3aa164d10(eventobject) {
        var self = this;
        return self.onClickNextButton.call(this);
    },
    /** preShow defined for frmPoliceView **/
    AS_Form_a890e7db8588444aaa0fc0c6571c2cee: function AS_Form_a890e7db8588444aaa0fc0c6571c2cee(eventobject) {
        var self = this;
        return self.preShowPoliceView.call(this);
    }
});