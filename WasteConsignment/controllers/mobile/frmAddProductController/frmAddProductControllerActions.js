define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchBackArrow defined for appHeader **/
    AS_UWI_a1b0eac58cd845d4b86a39c34145fcd1: function AS_UWI_a1b0eac58cd845d4b86a39c34145fcd1(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** onSelection defined for lstBxAddProduct **/
    AS_ListBox_c024f71fcd5c4658bf3e1366f8606335: function AS_ListBox_c024f71fcd5c4658bf3e1366f8606335(eventobject) {
        var self = this;
        this.onSelectProduct();
    },
    /** onTextChange defined for txtQuantity **/
    AS_TextField_e8b9fa7eaaa54bc1be82234888470c9f: function AS_TextField_e8b9fa7eaaa54bc1be82234888470c9f(eventobject, changedtext) {
        var self = this;
        this.onDoneEnterQuantity();
    },
    /** onTouchStart defined for txtQuantity **/
    AS_TextField_c06f432a2e5243ce9500b9da4ae4eaee: function AS_TextField_c06f432a2e5243ce9500b9da4ae4eaee(eventobject, x, y) {
        var self = this;
        this.onEnterQuantity();
    },
    /** onBeginEditing defined for txtQuantity **/
    AS_TextField_h46412ec3c5f4d659c40661b72c20ac2: function AS_TextField_h46412ec3c5f4d659c40661b72c20ac2(eventobject, changedtext) {
        var self = this;
        this.onEnterQuantity();
    },
    /** onClick defined for btnAddProduct **/
    AS_Button_i168547edc90466e8aa2ea1bffd79c27: function AS_Button_i168547edc90466e8aa2ea1bffd79c27(eventobject) {
        var self = this;
        this.onChooseAddProduct();
    },
    /** onClick defined for btnCancel **/
    AS_Button_f2169428fe0a478f97cd738a90c7b43d: function AS_Button_f2169428fe0a478f97cd738a90c7b43d(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** preShow defined for frmAddProduct **/
    AS_Form_gcb0843d655548898f2e9606f5571057: function AS_Form_gcb0843d655548898f2e9606f5571057(eventobject) {
        var self = this;
        this.onPreshowAddProduct();
    }
});