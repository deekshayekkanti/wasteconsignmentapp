 
define({ 
  
   _existingProd : [],
  
    onNavigate : function(params){
          _existingProd = params["productData"];
          printObject(params ,"Navigation to Add Product");
        },    
  
      onPreshowAddProduct : function(){
          printObject("onPreshowAddProduct");
          this.view.appHeader.headerText = "Add Product";
          this.view.lstBxAddProduct.skin = "sknLstBlack140TxtOpacity87";
          this.view.btnAddProduct.skin = "sknBtnBlueBG150TxtOpacity38";
          this.view.lstBxAddProduct.selectedKey = "select";
          this.view.txtQuantity.setEnabled(false);
          this.view.txtQuantity.text = "Enter Quantity";
          this.view.txtQuantity.placeholder = "";    
          this.setDataForProductsToAdd();
      },
  
    setDataForProductsToAdd : function(){
      printObject(_existingProd, "setDataForProductsToAdd");
      var lstData = [];
      lstData.push(["select" , "Select Product To Add"]);
      var productsToAdd = [ "Photgraphic Developer Fluid (PDF)",
                                       "Residue from dental practice containing amalgam (BGO)",
                                       "Human infectious soft clinical waste (SDS)"  
                                      ];
      var prodCount = productsToAdd.length;
      var key = "";
      for(var index = 0; index<prodCount; index++)
        {
          if(_existingProd.indexOf(productsToAdd[index]) == -1){
                       key = "product"+index ;
                       lstData.push([ key , productsToAdd[index]]);
          }
        }  
      printObject(lstData,"Master data for add products")
      this.view.lstBxAddProduct.masterData = lstData;
      this.view.lstBxAddProduct.selectedKey = "select";
      
    },

    onEnterQuantity : function(){
           printObject("Inside onEnterQuantity");
           var enteredQuantity = this.view.txtQuantity.text;
            if(enteredQuantity == "Enter Quantity"){
               this.view.txtQuantity.text = "";
               this.view.txtQuantity.placeholder = "Enter Quantity"; 
            }     
        },

        onDoneEnterQuantity : function(){
           printObject("Inside onDoneEnterQuantity");
           this.checkToEnableAddProduct();
        },

       onSelectProduct : function(){
          printObject("Inside onSelectProduct");
          var slctdProduct = this.view.lstBxAddProduct.selectedKey;
          var enteredQuantity = this.view.txtQuantity.text;
          this.view.lstBxAddProduct.skin = "sknLstLightGrey140";
          if(slctdProduct != "Select"){
            this.view.txtQuantity.setEnabled(true); 
            this.view.txtQuantity.setFocus(true);
            if(isEmpty(enteredQuantity)){
                  this.view.txtQuantity.text = "Enter Quantity";
              }
          }
          this.checkToEnableAddProduct();
       },

        checkToEnableAddProduct : function(){
            printObject("Inside checkToEnableAddProduct");
            var enteredQuantity = this.view.txtQuantity.text;
             var slctdProduct = this.view.lstBxAddProduct.selectedKey;
             if(! isEmpty(enteredQuantity) && enteredQuantity != "Enter Quantity" && slctdProduct != "select"){
               printObject(enteredQuantity+slctdProduct, "AddProductEnable");
              this.view.btnAddProduct.skin = "sknBtnBlueBG150";          
            }
            else{
              this.view.btnAddProduct.skin = "sknBtnBlueBG150TxtOpacity38";
            }
        },
  
      onEndEnterQuantity : function(){
        printObject("Inside onEndEnterQuantity");
        var enteredQuantity = this.view.txtQuantity.text;
         if(! isEmpty(enteredQuantity) && enteredQuantity.indexOf("unit") == -1){
              this.view.txtQuantity.text = enteredQuantity + "  unit" ;
         }        
      },

       onClickCancel : function(){
          printObject("Inside onClickCancel");
          var params = {}; 
          navigateToForm("frmWasteSummary", params); 
       },

        onChooseAddProduct : function(){
          printObject("Inside navigateToWasteSummary");
          var slctdProdValue = this.view.lstBxAddProduct.selectedKeyValue[1];
          var enteredQuantity = this.view.txtQuantity.text;
          var addedProduct = { "lblProductTxt" : "Product",
                                             "lblEWCC0de" : "EWC Code",
                                             "lblWeightTxt" : "Weight",
                                             "lblContainerTxt" : "Container",
                                             "lblQuantityTxt" : "Quantity",
                                            "lblProductDesc" : slctdProdValue , 
                                            "lblQuantityValue" : enteredQuantity , 
                                            "lblEWCValue":"090103" , 
                                            "lblWeightValue" : "20 KGs",
                                            "lblContainerValue" : "20 ltr Opaque Plastic Drum",
                                           "imgDelete" : { isVisible : true , src:"delete.png"}
                                          };
          var arrExistingProducts = gblInprogressVisit.HWNProduct;
          arrExistingProducts.push({           
                                "ProductCode": "090103",
                                "productDesc": slctdProdValue,
                                "Quantity": enteredQuantity,
                                "Container": "20 ltr Opaque Plastic Drum",
                                "Weight": "20 Kgs",
                                "ExpectedProduct": true
                              });
          gblInprogressVisit.HWNProduct = arrExistingProducts;
          var params = {"addedProduct":addedProduct}; 
          navigateToForm("frmWasteSummary", params); 
      },

  }); 