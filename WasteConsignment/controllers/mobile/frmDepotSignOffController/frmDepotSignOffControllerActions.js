define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgSignature **/
    AS_Image_cec9dea093684890a5de7272d251e7c1: function AS_Image_cec9dea093684890a5de7272d251e7c1(eventobject, x, y, context) {
        var self = this;
        return self.captureCustomerSignature.call(this);
    },
    /** onTouchBackArrow defined for appHeader **/
    AS_UWI_ja4239d285254d24a44317fa81a5f295: function AS_UWI_ja4239d285254d24a44317fa81a5f295(eventobject) {
        var self = this;

        function MOVE_ACTION____b620c6ec56c74d5b9327b4d37c65c4bb_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____b620c6ec56c74d5b9327b4d37c65c4bb_Callback
        });
    },
    /** onTextChange defined for txtSupervisorEmail **/
    AS_TextField_ddacc2b5628f41fe9c53879f77aeaf33: function AS_TextField_ddacc2b5628f41fe9c53879f77aeaf33(eventobject, changedtext) {
        var self = this;
        this.enableSignature();
    },
    /** onDone defined for txtSupervisorEmail **/
    AS_TextField_fb9587c512d348c18c9e1d60a2e5ab8f: function AS_TextField_fb9587c512d348c18c9e1d60a2e5ab8f(eventobject, changedtext, context) {
        var self = this;
        return self.onEnterEmail.call(this);
    },
    /** onClick defined for btnDelete **/
    AS_Button_g45a47ee500f4788b5d05fa05a8b647b: function AS_Button_g45a47ee500f4788b5d05fa05a8b647b(eventobject, context) {
        var self = this;
        return self.deleteSignature.call(this);
    },
    /** onClick defined for btnAcceptWaste **/
    AS_Button_e9d8dfcd898142e8bff6a0faec0d20e4: function AS_Button_e9d8dfcd898142e8bff6a0faec0d20e4(eventobject) {
        var self = this;
        this.showConfirmSignAlert();
    },
    /** onClickPrev defined for formPagination **/
    AS_UWI_cb6669b40e1040a186dd96836a727a97: function AS_UWI_cb6669b40e1040a186dd96836a727a97(eventobject) {
        var self = this;
        return self.onClickPrevPage.call(this);
    },
    /** onClickLeftArw defined for formPagination **/
    AS_UWI_a8599ae1774c490ebab642f90c2095d3: function AS_UWI_a8599ae1774c490ebab642f90c2095d3(eventobject) {
        var self = this;
        return self.onClickPrevPage.call(this);
    },
    /** leftBtnOnClick defined for onBoardConfirm **/
    AS_UWI_b36a23c291e34a4990eba4ebb6ab53c6: function AS_UWI_b36a23c291e34a4990eba4ebb6ab53c6(eventobject) {
        var self = this;
        this.onCancelSignOff();
    },
    /** rightBtnOnClick defined for onBoardConfirm **/
    AS_UWI_a293cb19cebc4ef0bd66e2f4718858b4: function AS_UWI_a293cb19cebc4ef0bd66e2f4718858b4(eventobject) {
        var self = this;
        this.onConfirmSignOff();
    },
    /** hideMenu defined for burgerMenu **/
    AS_UWI_a9d4a5fbabec4efb8fc3d3714113aff7: function AS_UWI_a9d4a5fbabec4efb8fc3d3714113aff7(eventobject) {
        var self = this;

        function MOVE_ACTION____f29d89db674f4c038db7585aa6b62339_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____f29d89db674f4c038db7585aa6b62339_Callback
        });
    },
    /** postShow defined for frmDepotSignOff **/
    AS_Form_h1c827ebd15e4b2cb906f06f29488be2: function AS_Form_h1c827ebd15e4b2cb906f06f29488be2(eventobject) {
        var self = this;
        this.postShowFrmDepotDropOff();
    }
});