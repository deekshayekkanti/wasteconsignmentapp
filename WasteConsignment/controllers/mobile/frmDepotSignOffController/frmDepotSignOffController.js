define({ 
  
  params : {},
  totalConsignment : 0,
  accptdConsignment : 0,
   
   onNavigate : function(params){     
     	printObject("Inside captureCustomerSignature");   
        this.params = params;
        this.updateDepotInfo();
        this.view.onBoardConfirm.isVisible = false;
        if(params.pageNum == 1){
           this.view.formPagination.isPrevImgVisible = false;
           this.view.formPagination.isPrevVisible = false;
        }else{
           this.view.formPagination.isPrevImgVisible = true;
           this.view.formPagination.isPrevVisible = true;
        }
        this.view.formPagination.pageNum = params.pageNum + " / " + params.pageCount;
        if(isEmpty(gblsupervisorSign)){
          this.deleteSignature(); 
        }else{
          this.updateSignature();
        }
        var depotDropOffModel  = require("depotDropOffModel");
        var signOffCount       = depotDropOffModel.getAcceptedConsignmentCount();
        this.accptdConsignment = signOffCount.accptdConsignment;
        this.totalConsignment  = signOffCount.totalConsignment;
        var consignmentNote    = this.accptdConsignment + " of " + this.totalConsignment + " Waste Consignment notes will be signed off";
        this.view.lblConsignmentNote.text = consignmentNote;      
    },

  postShowFrmDepotDropOff : function(){
    printObject("Inside postShowFrmDepotDropOff"); 
    this.view.txtSupervisorEmail.setFocus(true);
  },
  
  showConfirmSignAlert : function(){
     printObject("showConfirmSignAlert");
    if(this.view.flxAcceptWaste.skin == "sknFlxBlueBG"){
      if(this.accptdConsignment < this.totalConsignment){
         var consignmentNote = "You are about to accept only "+this.accptdConsignment + " of " + this.totalConsignment;
         consignmentNote += " consignments from this load. Please confirm you wish to proceed"
         this.view.onBoardConfirm.alertText = consignmentNote;
         this.view.onBoardConfirm.isVisible = true;
     }else{
        this.view.onBoardConfirm.isVisible = false;
        this.onConfirmSignOff();
     }
    }      
  },
  
  onCancelSignOff : function(){
    printObject("onCancelSignOff");
    this.view.onBoardConfirm.isVisible = false;
  },
  
  onConfirmSignOff : function(){
    printObject("onConfirmSignOff"); 
    var totalCount = gblConsignmentData.length;
    var totalConsignmentCount = 0;
    var consignmentStatus = "";
    var indexToDelete = [];
      for(var index=0; index< totalCount ; index++){
        consignmentStatus = gblConsignmentData[index].HWNDocument.WasteAcceptance;
        if(consignmentStatus == "accept" || consignmentStatus == "reject"){
              totalConsignmentCount++;
              indexToDelete.push(index);            
        }
      }
      var deleteCount = indexToDelete.length;
      for( index = 0;index < deleteCount; index++){
        gblConsignmentData.splice(indexToDelete[index],1);
      }
      
      if(totalConsignmentCount == totalCount){
        gblConsignmentData = []; 
        saveValueInKonyStore("gblConsignmentData",gblConsignmentData);
      }
      gblsupervisorSign = "";
      this.view.txtSupervisorEmail.text = "";
      this.navigateToHome();
  },
  
  navigateToHome : function(){
    printObject("Inside navigateToHome");
    var homeObj = new kony.mvc.Navigation("frmHome");
    homeObj.navigate({});
  },
  
  updateDepotInfo : function(){
        printObject(gblSelectedDepot,"Inside updateDepotInfo"); 
        this.view.lblName.text = gblSelectedDepot.lblName;
        this.view.lblAddressLine1.text = gblSelectedDepot.lblAddressLine1;
        this.view.lblAddressLine2.text = gblSelectedDepot.lblAddressLine2;
        this.view.lblPostCode.text = gblSelectedDepot.lblPostCode;
        this.view.lblCity.text = gblSelectedDepot.lblCity;
  },
  
  onClickPrevPage : function(){
    printObject("Inside onClickPrevPage");
    var params = {};
    params.pageNum = this.params.pageNum + 1;
    params.pageCount = this.params.pageCount ;
    var navigateObj = new kony.mvc.Navigation("frmDepotAcceptance");
    navigateObj.navigate(params);
  },
    
  onEnterEmail : function(){
    printObject("Inside onEnterEmail");
    this.enableSignature();    
  },
captureCustomerSignature: function() {
    
  	printObject("Inside captureCustomerSignature");
    var strimg 				= "";
    var SignHere			= "SignHere";
    var Clear				= "Clear";
    var NewBtn				= "Save";
  	var isCustomerRefAvail  = false;
  	var customerRefHint    	= "123";
  	var DateFormat			= "MM-dd-yyyy";
  	var TimeFormat			= "HH:mm:ss";
    var customerReference 	= "123";
    
    try {
        signatureFFI.getSignature(this.signCallBack, strimg, SignHere, Clear, NewBtn, isCustomerRefAvail, customerRefHint, customerReference, DateFormat, TimeFormat);  
    } catch (error) {
        printObject(error, "Inside onClickhboxSign - Exception - ");
    }
  },
  
  signCallBack: function(base64String, unScaledBase64, customerReference){
    try{
      	printObject(base64String,"Inside signCallBack");
      	gblsupervisorSign			=  base64String;
      	printObject(gblsupervisorSign,"Inside signCallBack  params");
      	this.updateSignature();
    } catch (error) {
      	printObject(error, "Inside signCallBack - Exception - ");
    }
  },
  
  enableSignature : function(){
    printObject("Inside enableSignature");
    var enteredEmail = this.view.txtSupervisorEmail.text;
    if(isEmpty(enteredEmail)){
      if(! isEmpty(gblsupervisorSign)){
          this.view.lblSignHere.skin = "sknLblLightGrey150";
          this.view.imgSignature.setVisibility(false);
      }else{
        
      }   
    }else{
      
      if(this.accptdConsignment >= 1){
         this.view.lblSignHere.skin = "sknLblBlack150";
         this.view.imgSignature.setVisibility(true);
          if( ! isEmpty(gblsupervisorSign)){
             this.view.imgSignature.base64 = gblsupervisorSign;
          }else{
            this.view.imgSignature.src = "imgtrans.png";
          }
      }
    }
  },
  
  updateSignature: function(){
    try {
      printObject("Inside updateSignature");
      var refreshObj  = {
                       "lblSignText"        : "",
                       "deleteSkin"         : "sknBtnDeleteEnabled" ,
                       "imgSignVisibility"  : true,
          			   "strBase64"          : gblsupervisorSign,
                       "flxAcceptSkin"      : "sknFlxBlueBG",
                       "btnAcceptSkin"      : "sknBtnTxtWhite150",
                       "flxAcceptEnable"    :  true,
                       "btnAcceptEnable"    :  true
                     }
      this.refreshData(refreshObj);
    } catch(exception){
      printObject(exception, "Inside updateCustomerSignature exception");
    }
  },
  
  deleteSignature: function(){
    try{
    	printObject("Inside deleteSignature");
        gblsupervisorSign = "";
        var email = this.view.txtSupervisorEmail.text;
        var params = {
                       "lblSignText"        : "TAP HERE TO SIGN",
                       "deleteSkin"         : "sknBtnDeleteDisabled" ,
                       "imgSignVisibility"  : isEmpty(email)?false:true,
          			   "strBase64"          : gblsupervisorSign,
                       "flxAcceptSkin"      : "sknFlxTransparentBlueBG",
                       "btnAcceptSkin"      : "sknBtnWhite150TxtOpacity38",
                       "flxAcceptEnable"    :  false,
                       "btnAcceptEnable"    :  false
                     }
        this.refreshData(params);
    } catch (error) {
        printObject(error, "Inside deleteSignature - Exception - ");
    }
  },
  
  refreshData : function(params){
        printObject(params, "refresh Signature")
        this.view.lblSignHere.text        = params.lblSignText;
        this.view.btnDelete.skin		  = params.deleteSkin;
        this.view.imgSignature.isVisible  = params.imgSignVisibility;
        this.view.flxAcceptWaste.skin     = params.flxAcceptSkin;
        this.view.btnAcceptWaste.skin     = params.btnAcceptSkin;
        this.view.flxAcceptWaste.setEnabled(params.flxAcceptEnable);
        //this.view.btnAcceptWaste.setEnabled(params.btnAcceptEnable);
         if(isEmpty(gblsupervisorSign)){
           this.view.imgSignature.src = "imgtrans.png";
           //this.view.imgSignature.base64 = "";
         }else{
           //this.view.imgSignature.src = ""
           this.view.imgSignature.base64 = gblsupervisorSign;
         }
  }

 });