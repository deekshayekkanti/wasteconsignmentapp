define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSelectDepot **/
    AS_Button_ha1cb136e8be433da44cf3563118c0e9: function AS_Button_ha1cb136e8be433da44cf3563118c0e9(eventobject, context) {
        var self = this;
        return self.onSelectDepot.call(this, eventobject, context);
    }
});