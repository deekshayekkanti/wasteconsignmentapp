define({ 
  
  onNavigate : function(){
    printObject("onNavigate to Home");
    this.updateConsignment();
  },
  
  updateConsignment : function(){
    printObject("updateConsignment");
    var consignmentCount = Number(gblConsignmentData.length);    
    this.view.lblConsignmentCount.text = consignmentCount + "";    
    if(consignmentCount === 0){
      this.view.btnStartDepotOff.skin = "sknBtnBlueBG150TxtOpacity38";
      this.view.btnStartDepotOff.focusSkin = "sknBtnBlueBG150TxtOpacity38";
      this.view.btnPoliceView.skin="sknBtnGrey150Opacity33";
      this.view.btnPoliceView.setEnabled(false);
    }
    else{
      this.view.btnStartDepotOff.skin = "sknBtnBlueBG150";
      this.view.btnStartDepotOff.focusSkin = "sknBtnBlueBG150";
      this.view.btnStartDepotOff.setEnabled(true);
      this.view.btnPoliceView.skin="sknBtnBlueTxt150";
      this.view.btnPoliceView.setEnabled(true);
    }
    
  },
  
  navigateToDepotDropOff : function(){
    printObject("Inside navigateToDepotDropOff");
    if(this.view.btnStartDepotOff.skin != "sknBtnBlueBG150TxtOpacity38"){
       var params = {};
       var navigateObj = new kony.mvc.Navigation("frmDepotSelection");
       navigateObj.navigate(params);
    }
  },
   
 //Type your controller code here 
  navigateToPoliceView: function(){
    printObject("Inside navigateToPoliceView");
    var params = {};
    var navigateObj = new kony.mvc.Navigation("frmPoliceView");
    navigateObj.navigate(params);
  }
 });