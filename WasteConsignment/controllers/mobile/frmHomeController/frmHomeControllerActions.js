define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchBackArrow defined for appHeader **/
    AS_UWI_c1931f45f65a4f1988916efbc7813c6b: function AS_UWI_c1931f45f65a4f1988916efbc7813c6b(eventobject) {
        var self = this;

        function MOVE_ACTION____f43d4f7b755c4c118b7642250e04f3d6_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____f43d4f7b755c4c118b7642250e04f3d6_Callback
        });
    },
    /** onClick defined for btnStartDepotOff **/
    AS_Button_e56b116784ac42c8a79fcceb0044d987: function AS_Button_e56b116784ac42c8a79fcceb0044d987(eventobject) {
        var self = this;
        return self.navigateToDepotDropOff.call(this);
    },
    /** onClick defined for btnPoliceView **/
    AS_Button_i3dd0e96f6bc480eb9df6e17f9f61fde: function AS_Button_i3dd0e96f6bc480eb9df6e17f9f61fde(eventobject) {
        var self = this;
        return self.navigateToPoliceView.call(this);
    },
    /** hideMenu defined for burgerMenu **/
    AS_UWI_a24fa0b3af814e3fb8e47fba6d5f28ce: function AS_UWI_a24fa0b3af814e3fb8e47fba6d5f28ce(eventobject) {
        var self = this;

        function MOVE_ACTION____b11149a99e454d81aef22bbfe0ac66f1_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____b11149a99e454d81aef22bbfe0ac66f1_Callback
        });
    }
});