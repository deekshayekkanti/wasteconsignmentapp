define({ 
  
  onNavigate:function(){
    printObject("Inside on Navigate to Depot Selection");
  },
  
  onPreShowDepotSelection : function(){
    this.view.onBoardTwo.isVisible = false;
  },

  onSelectDepot : function(eventObject){   
    printObject(eventObject,"Depot Selection");
    var depotData = this.view.segDepotSelection.data;
    gblSelectedDepot = depotData[eventObject.selectedRowIndex[1]]; 
    printObject(gblSelectedDepot,"Selected Depot");
    this.view.onBoardTwo.isVisible = true;
  },
  
  onClickAlertOk : function(){
    printObject("onClickAlertOk");
    var params = {};
    navigateToForm("frmDepotAcceptance", params);  
 }
  
 });