define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchBackArrow defined for appHeader **/
    AS_UWI_ifd94c0a03aa402badec526a16c5b2a1: function AS_UWI_ifd94c0a03aa402badec526a16c5b2a1(eventobject) {
        var self = this;

        function MOVE_ACTION____add1cadd31ea450f92515c1c4929659c_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____add1cadd31ea450f92515c1c4929659c_Callback
        });
    },
    /** btnOKClick defined for onBoardTwo **/
    AS_UWI_jae60268d9cd4a04858719ca209133ee: function AS_UWI_jae60268d9cd4a04858719ca209133ee(eventobject) {
        var self = this;
        return self.onClickAlertOk.call(this);
    },
    /** hideMenu defined for burgerMenu **/
    AS_UWI_jba385000663482490697b4807635e21: function AS_UWI_jba385000663482490697b4807635e21(eventobject) {
        var self = this;

        function MOVE_ACTION____c023dc1778804cd293c410750dcbc35e_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____c023dc1778804cd293c410750dcbc35e_Callback
        });
    },
    /** preShow defined for frmDepotSelection **/
    AS_Form_a0e36993e8ca4967a097ac09d8004eca: function AS_Form_a0e36993e8ca4967a097ac09d8004eca(eventobject) {
        var self = this;
        this.onPreShowDepotSelection();
    }
});