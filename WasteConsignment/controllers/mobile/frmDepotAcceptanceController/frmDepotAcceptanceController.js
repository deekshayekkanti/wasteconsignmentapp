define({ 

  slctdPage : 0,
  consignmentCount : Number(gblConsignmentData.length),
  
  onNavigate : function(params){
    printObject(params,"OnNavigate to Depot Acceptance");
    printObject(this.slctdPage, "selecte page");
    if(! isEmpty(params)){
      this.slctdPage--;
    }else{
      this.slctdPage = 0;
    }
    this.updatePage();
  },
  
  updatePage : function(){
     printObject(this.slctdPage,"updatePageNum");
     printObject(this.consignmentCount,"updatePageNum");
     this.consignmentCount = Number(gblConsignmentData.length)
     var pageNum  =  this.slctdPage + 1;
     var pageCount = this.consignmentCount + 1;
     this.view.formPagination.pageNum = pageNum + " / " + pageCount;
     this.showOrHidePrevOrNext();
  },
  
  showOrHidePrevOrNext : function(){
    printObject("showOrHidePrevOrNext");
    var currentPage = this.slctdPage + 1;
    if(currentPage == 1){
       this.view.formPagination.isPrevVisible = false;
       this.view.formPagination.isPrevImgVisible = false;
    }else{
      this.view.formPagination.isPrevVisible = true;
      this.view.formPagination.isPrevImgVisible = true;
    }
    if(currentPage <= this.consignmentCount){
      this.onChangePage();
    }else{
          var params = {};
          params.pageNum = currentPage;
          params.pageCount = this.consignmentCount + 1 ;
          var navigateObj = new kony.mvc.Navigation("frmDepotSignOff");
          navigateObj.navigate(params); 
    }

  },
  
  onClickPrevPage : function(){
   printObject("onClickPrevPage");
   this.slctdPage--;
   this.updatePage(); 
  },
  
  onClickNextPage : function(){
    printObject("onClickNextPage");
    this.slctdPage++;
    this.updatePage();
  },
  
  onChangePage : function(){
    printObject(gblConsignmentData[this.slctdPage], "onChangePage");
    var slctdConsignmentNo       = gblConsignmentData[this.slctdPage].HWNDocument.WCNNumber;
    var slctdConsignmentAddress  = gblConsignmentData[this.slctdPage].HWNCustomer;
    var slctdConsignmentproduct  = gblConsignmentData[this.slctdPage].HWNProduct;
    var WasteSummaryModel        = require("wasteSummaryModel");
    var segData                  = WasteSummaryModel.constructWasteSummarySegmentData(slctdConsignmentAddress,slctdConsignmentproduct,"flxDepotDropOff",false);
    printObject(segData, "Selected Consignment Data");
    this.loadConsignmentStatus();
    this.view.lblConsignmentValue.text = slctdConsignmentNo;
    this.view.segWasteAcceptance.widgetDataMap = WasteSummaryModel.constructWidgetDataMap();
    this.view.segWasteAcceptance.setData(segData);  
    this.view.segWasteAcceptance.scrollsToTop = true;
  },
  
  loadConsignmentStatus : function(){
    var consignmentStatus = gblConsignmentData[this.slctdPage].HWNDocument.WasteAcceptance;
    printObject(consignmentStatus, "loadConsignmentStatus");
    if(consignmentStatus == ""){
        this.view.flxAcceptWaste.skin = "sknFlxBlueBG";
        this.view.imgAccept.isVisible = false;
        this.view.btnRejectWaste.setEnabled(true);
        this.view.btnRejectWaste.skin = "sknBtnGrey150Opacity54";
      }else{
        this.view.flxAcceptWaste.skin = "sknFlxGreenBG";
        this.view.imgAccept.isVisible = true;
        this.view.btnRejectWaste.setEnabled(false);
        this.view.btnRejectWaste.skin = "sknBtnGrey150Opacity33";
      }
  },
    
  updateConsignmentStatus : function(){
    var consignmentStatus = gblConsignmentData[this.slctdPage].HWNDocument.WasteAcceptance;
    printObject(consignmentStatus,"updateConsignmentStatus");
    if(consignmentStatus == ""){
        this.view.flxAcceptWaste.skin = "sknFlxGreenBG";
        this.view.imgAccept.isVisible = true;
        this.view.btnRejectWaste.setEnabled(false);
        this.view.btnRejectWaste.skin = "sknBtnGrey150Opacity33";
        slctdConsignmntHdr = "accept";
      }else{
        this.view.flxAcceptWaste.skin = "sknFlxBlueBG";
        this.view.imgAccept.isVisible = false;
        this.view.btnRejectWaste.setEnabled(true);
        this.view.btnRejectWaste.skin = "sknBtnGrey150Opacity54";
        slctdConsignmntHdr = "";
      }
    return slctdConsignmntHdr;
  },
  
  onAcceptOrRejectWaste : function(eventObject){
    printObject(gblConsignmentData[this.slctdPage], "Before AcceptOrRejectWaste");
    printObject(eventObject, "Selected action object");
    var slctdConsignmentData = gblConsignmentData[this.slctdPage]; 
    var slctdConsignmntHdr   = slctdConsignmentData.HWNDocument.WasteAcceptance;
    if(eventObject.id == "flxAcceptWaste" || eventObject.id == "btnAcceptWaste"){
      slctdConsignmentData.HWNDocument.WasteAcceptance = this.updateConsignmentStatus();
      printObject(gblConsignmentData[this.slctdPage], "After AcceptOrRejectWaste");
    }else{
      this.view.onBoardConfirm.isVisible = true;
    }
    
  },
  
   onConfirmReject : function(){
     printObject(gblConsignmentData[this.slctdPage], "onConfirmReject");
     this.view.onBoardConfirm.isVisible = false;  
     gblConsignmentData.splice(this.slctdPage, 1);
     if(isEmpty(gblConsignmentData)){
       gblConsignmentData = [];
       navigateToForm("frmHome", {});
     }else{
       this.updatePage();
     }
    /*
     var slctdConsignmentData = gblConsignmentData[this.slctdPage]; 
     var slctdConsignmntHdr = "reject";
     slctdConsignmentData.HWNDocument.WasteAcceptance = slctdConsignmntHdr; 
     */
  },
  
  onCancelReject : function(){
    printObject("onCancelReject");
    this.view.onBoardConfirm.isVisible = false;  
  }
 //Type your controller code here 

 });