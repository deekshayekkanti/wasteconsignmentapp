define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchBackArrow defined for appHeader **/
    AS_UWI_be94a6d3c84a4215beeea7b6f1b383ac: function AS_UWI_be94a6d3c84a4215beeea7b6f1b383ac(eventobject) {
        var self = this;

        function MOVE_ACTION____e1a2c1b015f94e37967afe5ca5e423aa_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____e1a2c1b015f94e37967afe5ca5e423aa_Callback
        });
    },
    /** onClick defined for btnAcceptWaste **/
    AS_Button_f44331372f184f0a9a2185cefa53ae76: function AS_Button_f44331372f184f0a9a2185cefa53ae76(eventobject) {
        var self = this;
        return self.onAcceptOrRejectWaste.call(this, eventobject);
    },
    /** onClick defined for flxAcceptWaste **/
    AS_FlexContainer_g80ee645ae924902adb93c01bfab7e0f: function AS_FlexContainer_g80ee645ae924902adb93c01bfab7e0f(eventobject) {
        var self = this;
        return self.onAcceptOrRejectWaste.call(this, eventobject);
    },
    /** onClick defined for btnRejectWaste **/
    AS_Button_f94567b620ea446ca2ee866e26245860: function AS_Button_f94567b620ea446ca2ee866e26245860(eventobject) {
        var self = this;
        return self.onAcceptOrRejectWaste.call(this, eventobject);
    },
    /** onClickPrev defined for formPagination **/
    AS_UWI_c18e7f66cf9d48568f9be7a8fbbdb15b: function AS_UWI_c18e7f66cf9d48568f9be7a8fbbdb15b(eventobject) {
        var self = this;
        this.onClickPrevPage();
    },
    /** onClickNext defined for formPagination **/
    AS_UWI_f57cba79814249ed93d7501772bbe243: function AS_UWI_f57cba79814249ed93d7501772bbe243(eventobject) {
        var self = this;
        this.onClickNextPage();
    },
    /** onClickLeftArw defined for formPagination **/
    AS_UWI_a38318b2ab014a3aae6e8059c98c16ef: function AS_UWI_a38318b2ab014a3aae6e8059c98c16ef(eventobject) {
        var self = this;
        this.onClickPrevPage();
    },
    /** onClickRightArw defined for formPagination **/
    AS_UWI_c7ea19c3cc5d40eaa1fc53070699dd20: function AS_UWI_c7ea19c3cc5d40eaa1fc53070699dd20(eventobject) {
        var self = this;
        this.onClickNextPage();
    },
    /** leftBtnOnClick defined for onBoardConfirm **/
    AS_UWI_dc62f695ee694dbb99460a57f274c743: function AS_UWI_dc62f695ee694dbb99460a57f274c743(eventobject) {
        var self = this;
        this.onConfirmReject();
    },
    /** rightBtnOnClick defined for onBoardConfirm **/
    AS_UWI_i088f3b8c4ea47abb45336fa68c6dde8: function AS_UWI_i088f3b8c4ea47abb45336fa68c6dde8(eventobject) {
        var self = this;
        this.onCancelReject();
    },
    /** hideMenu defined for burgerMenu **/
    AS_UWI_b08939f435c6438ca72cb932c8744624: function AS_UWI_b08939f435c6438ca72cb932c8744624(eventobject) {
        var self = this;

        function MOVE_ACTION____a8fa76e94b8947da97359d4826b3346d_Callback() {}
        self.view.burgerMenu.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____a8fa76e94b8947da97359d4826b3346d_Callback
        });
    }
});