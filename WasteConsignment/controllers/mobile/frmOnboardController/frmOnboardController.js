define({ 
  
  _currentRow : 0,
  
  onPreShowOnBoardScreen : function(){
    var datObj = {};
    var segData = [];
  
  },
  
   onSwipeToNext : function(rowNumber){
     printObject(rowNumber, "onSwipeToNext");
     var numberOfRows = this.view.segOnBoard.data.length;
     if(rowNumber == this._currentRow && rowNumber == numberOfRows - 1){
       this.onClickSkip(this);
     }else{
       if(rowNumber == numberOfRows - 1){
         this.view.btnSwipe.text = "Open App";
       }else {
         this.view.btnSwipe.text = "Swipe to next screen";
       }
     }
      this._currentRow = rowNumber;  
   },
   
    onClickSkip : function(eventObject){
     saveValueInKonyStore("isOnboardShown", true);
     if(eventObject.id == "btnSwipe" && this.view.btnSwipe.text == "Swipe to next screen"){
       // do  nothing
     }else{
        navigateToForm("frmHome",{});  
     } 
     printObject(eventObject,"Inside onClickSkip in Onboard");
     //navigateToForm("frmWasteSummary",{});
   }
  
 });