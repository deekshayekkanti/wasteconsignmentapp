define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSkip **/
    AS_Button_ja14c2660cfd4b8ab99086f5ddc41841: function AS_Button_ja14c2660cfd4b8ab99086f5ddc41841(eventobject) {
        var self = this;
        return self.onClickSkip.call(this, eventobject);
    },
    /** onSwipe defined for segOnBoard **/
    AS_Segment_e997969275934585a57bc9d3f05255d2: function AS_Segment_e997969275934585a57bc9d3f05255d2(seguiWidget, sectionIndex, rowIndex) {
        var self = this;
        return self.onSwipeToNext.call(this, rowIndex);
    },
    /** onClick defined for btnSwipe **/
    AS_Button_a4454255c0384c35bdcd7e33fe144ca6: function AS_Button_a4454255c0384c35bdcd7e33fe144ca6(eventobject, context) {
        var self = this;
        return self.onClickSkip.call(this, eventobject);
    }
});